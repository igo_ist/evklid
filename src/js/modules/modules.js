window.addEventListener('DOMContentLoaded', function() {
  /* Вызов меню бургер */
  document.querySelector('#burger').addEventListener('click', function() {
    document.querySelector('#menu').classList.toggle('active')
    document.querySelector('#burger').classList.toggle('active')
  });
});



$(document).ready(function(){
  /* Скролл хедер */
  const $header = $("#header")
  let prevScroll
  let lastShowPos

  $(window).on("scroll", function() {
    const scrolled = $(window).scrollTop()

    if (scrolled > 100 && scrolled > prevScroll ) {
      $header.addClass("out")
      $("#menu").removeClass("active")
      $("#burger").removeClass("active")
      lastShowPos = scrolled
    } else if (scrolled <= Math.max(lastShowPos - 250, 0)) {
      $header.removeClass("out")
    }
    prevScroll = scrolled
  })
  /* Параметры Swiper */
  const swiper = new Swiper('.swiper-container', {
    lazy: true,
    loop: true,
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
    }
  });
  /* Табы */
  let tab = $('.work__step_btn');
  let tab_descr = $('.step');
  $(tab).click(function(){
    let tab_id = $(this).attr('data-tab');

    $(tab).removeClass('current');
    $(tab_descr).removeClass('current');

    $(this).addClass('current');
    $("#"+tab_id).addClass('current');
  });
  /* Аккордуон */
  let acc_btn = $(".questions__item_btn")
  let acc_descr = $('.questions__item_descr')
  $(acc_btn).on("click", function() {
    if ($(this).hasClass("active")) {

      $('.questions__item').removeClass('active');
      $('.questions__item_icon').removeClass('active');
      $(this).removeClass('active');
      $(this).stop(true, true)
        .siblings(acc_descr)
        .slideUp(400);
    } else {

      $('.questions__item').removeClass('active');
      $(acc_btn).removeClass('active');
      $('.questions__item_icon').removeClass('active');
      $(this).addClass('active');
      $(this).parents('.questions__item').addClass('active');
      $(this).children('.questions__item_icon').addClass('active');
      $(acc_descr).slideUp(400);
      $(this)
        .siblings(acc_descr)
        .slideDown(400);

    }
  });
});
